/* eslint-disable no-underscore-dangle */
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import isEmpty from "lodash/isEmpty";
import throttle from "lodash/throttle";

import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import * as actionCreators from "../../store/actions";

const style = (theme) => {
  // console.log(theme);
  return {
    root: {
      width: "90%",
      minWidth: 300,
      height: "auto",
      marginTop: theme.spacing(5),
      marginBottom: theme.spacing(5),
      margin: "auto",
      overflowX: "auto",
      overflowY: "",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
    },
    table: {
      minWidth: 300,
    },
    progress: {
      width: 100,
      margin: theme.spacing(2),
    },
    link: {
      textDecoration: "none",
      display: "inline-block",
      width: "100%",
      height: "100%",
      padding: "1em 0 1em 0",
    },
    infiniteLoaderContainer: {
      height: "auto",
    },
  };
};

class UserDashboard extends Component {
  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.root}>
        <h1 align="center">Dashboard</h1>
      </Paper>
    );
  }
}

UserDashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  userList: PropTypes.array.isRequired,
};
const mapStateToProps = (state) => {
  return {
    userList: !isEmpty(state.userList) ? state.userList : [],
  };
};
export default withStyles(style)(connect(mapStateToProps)(UserDashboard));
