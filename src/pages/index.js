import SignUp from './SignUp/SignUp';
import SignIn from './SignIn/SignIn';
import Home from './Home/Home';
import NoMatchPage from './NoMatchPage/NoMatchPage';
import UserDetail from './UserDetail/UserDetail';
import UserDashboard from './UserDashboard/UserDashboard';

export { SignIn, SignUp, UserDashboard, UserDetail, NoMatchPage, Home };
