/* eslint-disable no-prototype-builtins,no-restricted-syntax */
import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { LockOpenOutlined } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actionCreators from '../../store/actions/index';
import { formActions, formStyles } from '../../utils';

class SignUp extends React.Component {
  state = {
    completeFormValidity: false,
    errorMsg: 'Data is invalid, server denied, please check your data and retry',
    showErrMsg: false,
    formData: {
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Name',
        },
        value: '',
        label: 'Name',
        rules: {
          required: true,
          minLength: 6,
          maxLength: 30,
        },
        isValid: true,
        errorMsg: 'REQUIRED!! And should be between 4-18 characters',
        touched: false,
      },
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Your E-Mail',
        },
        value: '',
        label: 'Email',
        rules: {
          required: true,
          minLength: 8,
          maxLength: 30,
          isEmail: true,
        },
        isValid: true,
        errorMsg: 'REQUIRED!! And should be a valid email address',
        touched: false,
      },
      phone: {
        elementType: 'input',
        elementConfig: {
          type: 'number',
          placeholder: 'Your Contact No.',
        },
        value: '',
        label: 'Contact Number',
        rules: {
          required: true,
          minLength: 10,
          maxLength: 10,
          isNumeric: true,
        },
        isValid: true,
        errorMsg: 'REQUIRED!! And should be 10 characters long',
        touched: false,
      },
      password: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Your password',
        },
        value: '',
        label: 'Password',
        rules: {
          required: true,
          minLength: 6,
          maxLength: 40,
        },
        isValid: true,
        errorMsg: 'REQUIRED!! And should be between 6-40 characters',
        touched: false,
      },
      confirmPassword: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Confirm password',
        },
        value: '',
        label: 'Confirm Password',
        rules: {
          required: true,
          confirmPassword: true,
        },
        isValid: true,
        errorMsg: 'Must be same as above password',
        touched: false,
      },
    },
  };

  checkCompleteFormValidity = () => {
    let dataValid = true;
    // eslint-disable-next-line
    for (const key in this.state.formData) {
      // eslint-disable-next-line
      dataValid = this.state.formData[key].isValid && this.state.formData[key].touched && dataValid;
    }
    // console.log(dataValid);
    this.setState({ completeFormValidity: dataValid });
  };

  submitFormHandler = event => {
    const { completeFormValidity, formData } = this.state;
    const { dispatch, history } = this.props;
    event.preventDefault();
    // eslint-disable-next-line
    if (completeFormValidity) {
      const userDataFromState = {};
      for (const key in formData) {
        if (formData.hasOwnProperty(key)) {
          userDataFromState[key] = formData[key].value;
        }
      }
      // console.log(userDataFromState);
      this.setState({ loading: true }, async () => {
        try {
          const res = await dispatch(actionCreators.signUpUser(userDataFromState, history))
          // return from try block of signUp action
          if (res.payload.status === 200) {
            // eslint-disable-next-line
            this.setState({loading: false}, () => history.push('/sign_in'));
          } else {
            // return from catch block of signUp action
            console.log("else block")
            throw new Error(res.message)
          }
        }catch (e) {
          this.setState({ loading: false, showErrMsg: true, errorMsg: e.message });
        }


      });
    }
  };

  checkValidity = event => {
    // prevState => {} & cb  wont work bcoz of react synthetic event
    // eslint-disable-next-line
    this.setState(formActions.validation(event, this.state), () =>
      this.checkCompleteFormValidity(),
    );
  };

  onChangehandler = event => {
    // prevState => {} & cb  wont work bcoz of react synthetic event
    this.checkValidity(event);
    // eslint-disable-next-line
    this.setState(formActions.handleChange(event, this.state));
  };

  render() {
    const { classes } = this.props;
    const { formData, loading, errorMsg, showErrMsg } = this.state;
    let spinner;
    if (loading) {
      spinner = <CircularProgress color="secondary" size={20} thickness={2.6} />;
    }
    return (
      <React.Fragment>
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOpenOutlined />
            </Avatar>
            <Typography variant="h5">Sign up</Typography>
            <form onSubmit={this.submitFormHandler} className={classes.form}>
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="name">{formData.name.label}</InputLabel>
                <Input
                  id="name"
                  onBlur={this.checkValidity}
                  onChange={this.onChangehandler}
                  value={formData.name.value}
                  placeholder={formData.name.elementConfig.placeholder}
                  name="name"
                  error={!formData.name.isValid && formData.name.touched}
                  autoComplete="name"
                  autoFocus
                />
                <span className={classes.errMsg}>
                  {!formData.name.isValid && formData.name.touched ? formData.name.errorMsg : null}
                </span>
              </FormControl>
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="email">{formData.email.label}</InputLabel>
                <Input
                  id="email"
                  onBlur={this.checkValidity}
                  onChange={this.onChangehandler}
                  value={formData.email.value}
                  placeholder={formData.email.elementConfig.placeholder}
                  name="email"
                  error={!formData.email.isValid && formData.email.touched}
                  autoComplete="email"
                />
                <span className={classes.errMsg}>
                  {!formData.email.isValid && formData.email.touched
                    ? formData.email.errorMsg
                    : null}
                </span>
              </FormControl>
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="phone">{formData.phone.label}</InputLabel>
                <Input
                  id="phone"
                  onBlur={this.checkValidity}
                  onChange={this.onChangehandler}
                  type="number"
                  value={formData.phone.value}
                  placeholder={formData.phone.elementConfig.placeholder}
                  name="phone"
                  error={!formData.phone.isValid && formData.phone.touched}
                  autoComplete="phone"
                />
                <span className={classes.errMsg}>
                  {!formData.phone.isValid && formData.phone.touched
                    ? formData.phone.errorMsg
                    : null}
                </span>
              </FormControl>
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="password">{formData.password.label}</InputLabel>
                <Input
                  name="password"
                  onBlur={this.checkValidity}
                  onChange={this.onChangehandler}
                  value={formData.password.value}
                  placeholder={formData.password.elementConfig.placeholder}
                  error={!formData.password.isValid && formData.password.touched}
                  type="password"
                  id="password"
                  autoComplete="current-password"
                />
                <span className={classes.errMsg}>
                  {!formData.password.isValid && formData.password.touched
                    ? formData.password.errorMsg
                    : null}
                </span>
              </FormControl>
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="confirmPassword">{formData.confirmPassword.label}</InputLabel>
                <Input
                  name="confirmPassword"
                  onBlur={this.checkValidity}
                  onChange={this.onChangehandler}
                  value={formData.confirmPassword.value}
                  placeholder={formData.confirmPassword.elementConfig.placeholder}
                  error={!formData.confirmPassword.isValid && formData.confirmPassword.touched}
                  type="password"
                  id="confirmPassword"
                  autoComplete="current-password"
                />
                <span className={classes.errMsg}>
                  {!formData.confirmPassword.isValid && formData.confirmPassword.touched
                    ? formData.confirmPassword.errorMsg
                    : null}
                </span>
              </FormControl>
              <span className={classes.errMsg}>{showErrMsg ? errorMsg : null}</span>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                <Grid container justify="center">
                  <Grid item xs={loading ? 7 : 12}>
                    Sign up
                  </Grid>
                  <Grid item xs={loading ? 3 : 1}>
                    {spinner}
                  </Grid>
                </Grid>
              </Button>
            </form>
            <div className={classes.linkToRegister}>
              <Typography variant="body1" style={{ margin: '20px 0 0 0' }}>
                Already sign up?
              </Typography>
              <Typography variant="body1" style={{ margin: '0' }}>
                Please switch to Sign in
              </Typography>
              <Link to="/sign_in" className={classes.submit}>
                <Button variant="contained" color="secondary">
                  Sign in
                </Button>
              </Link>
            </div>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

SignUp.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default withStyles(formStyles)(connect()(SignUp));
