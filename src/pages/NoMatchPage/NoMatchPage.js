import React from 'react';

const noMatchPage = () => {
  return (
    <div>
      <h2 align="center">Sorry nothing found :( !!</h2>
    </div>
  );
};

export default noMatchPage;
