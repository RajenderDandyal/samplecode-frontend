/* eslint-disable no-prototype-builtins,no-restricted-syntax,no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { LockOpenOutlined } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';

import * as actionCreators from '../../store/actions';
import { authToken, formActions, formStyles } from '../../utils';
import * as actionTypes from '../../store/actions/actionTypes';

class SignIn extends React.Component {
  state = {
    completeFormValidity: false,
    errorMsg: 'Data is invalid, server denied, please check your data and retry',
    showErrMsg: false,
    formData: {
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Your E-Mail',
        },
        value: '',
        label: 'Email',
        rules: {
          required: true,
          minLength: 8,
          maxLength: 30,
          isEmail: true,
        },
        isValid: true,
        errorMsg: 'REQUIRED!! And should be a valid email address',
        touched: false,
      },
      password: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Your password',
        },
        value: '',
        label: 'Password',
        rules: {
          required: true,
          minLength: 6,
          maxLength: 40,
        },
        isValid: true,
        errorMsg: 'REQUIRED!! And should be between 6-40 characters',
        touched: false,
      },
    },
  };

  checkCompleteFormValidity = () => {
    const { formData } = this.state;
    let dataValid = true;
    for (const key in formData) {
      if (formData.hasOwnProperty(key)) {
        dataValid = formData[key].isValid && formData[key].touched && dataValid;
      }
    }
    // console.log(dataValid);
    this.setState({ completeFormValidity: dataValid });
  };

  submitFormHandler = event => {
    event.preventDefault();
    const { formData, completeFormValidity } = this.state;
    const { dispatch, history } = this.props;
    if (completeFormValidity) {
      const userDataFromState = {};
      // eslint-disable-next-line prefer-const
      for (let key in formData) {
        if (formData.hasOwnProperty(key)) {
          userDataFromState[key] = formData[key].value;
        }
      }
      // console.log(userDataFromState);
      this.setState({ loading: true }, async () => {
        try {
          const tokenRes = await dispatch(actionCreators.signInUser(userDataFromState));
          // console.log(tokenRes);
          if (!isEmpty(tokenRes.payload) && tokenRes.type === actionTypes.SIGNIN_USER) {
            const tokenObj = tokenRes.payload.find(item => item.tokenFor === 'authentication');
            authToken.setAuthToken('jwt_auth', tokenObj.token);
            // console.log(tokenRes);
            await dispatch(actionCreators.userDetail(authToken.decodeToken(tokenObj.token)._id));
            this.setState({ loading: false }, () => history.push('/user_dashboard'));
          } else {
            throw new Error(tokenRes.message);
          }
        } catch (e) {
          // console.log(e);
          this.setState({ loading: false, showErrMsg: true, errorMsg: e.message });
        }
      });
    }
  };

  checkValidity = event => {
    // prevState => {} & cb  wont work bcoz of react synthetic event
    // eslint-disable-next-line
    this.setState(formActions.validation(event, this.state), () =>
      this.checkCompleteFormValidity(),
    );
  };

  onChangehandler = event => {
    // prevState => {} & cb  wont work bcoz of react synthetic event
    this.checkValidity(event);
    // eslint-disable-next-line
    this.setState(formActions.handleChange(event, this.state));
  };

  render() {
    const { classes } = this.props;
    const { formData, loading, errorMsg, showErrMsg } = this.state;
    let spinner;
    if (loading) {
      spinner = <CircularProgress color="secondary" size={20} thickness={2.6} />;
    }
    return (
      <React.Fragment>
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOpenOutlined />
            </Avatar>
            <Typography variant="h5">Sign in</Typography>
            <form onSubmit={this.submitFormHandler} className={classes.form}>
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="email">{formData.email.label}</InputLabel>
                <Input
                  id="email"
                  onBlur={this.checkValidity}
                  onChange={this.onChangehandler}
                  value={formData.email.value}
                  placeholder={formData.email.elementConfig.placeholder}
                  name="email"
                  error={!formData.email.isValid && formData.email.touched}
                  autoComplete="email"
                />
                <span className={classes.errMsg}>
                  {!formData.email.isValid && formData.email.touched
                    ? formData.email.errorMsg
                    : null}
                </span>
              </FormControl>
              <FormControl margin="normal" fullWidth>
                <InputLabel htmlFor="password">{formData.password.label}</InputLabel>
                <Input
                  name="password"
                  onBlur={this.checkValidity}
                  onChange={this.onChangehandler}
                  value={formData.password.value}
                  placeholder={formData.password.elementConfig.placeholder}
                  error={!formData.password.isValid && formData.password.touched}
                  type="password"
                  id="password"
                  autoComplete="current-password"
                />
                <span className={classes.errMsg}>
                  {!formData.password.isValid && formData.password.touched
                    ? formData.password.errorMsg
                    : null}
                </span>
              </FormControl>
              <span className={classes.errMsg}>{showErrMsg ? errorMsg : null}</span>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                <Grid container justify="center">
                  <Grid item xs={loading ? 7 : 12}>
                    Sign in
                  </Grid>
                  <Grid item xs={loading ? 3 : 1}>
                    {spinner}
                  </Grid>
                </Grid>
              </Button>
            </form>
            <div className={classes.linkToRegister}>
              <Typography variant="body1" style={{ margin: '20px 0 0 0' }}>
                Not a member yet?
              </Typography>
              <Typography variant="body1" style={{ margin: '0' }}>
                Please switch to Sign up
              </Typography>
              <Link to="/sign_up" className={classes.submit}>
                <Button variant="contained" color="secondary">
                  Sign up
                </Button>
              </Link>
            </div>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default withStyles(formStyles)(connect()(SignIn));
