import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: "95%",
    marginBottom: 10,
  },
  imgFullWidth: {
    width: "100%",
    height: window.innerHeight / 1.65,
  },
  img: {
    width: "100%",
    height: "100%",
  },
}));

function Home() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Grid container justify="center" className={classes.root}>
        <Grid item xs={12}>
          <img
            className={classes.imgFullWidth}
            src="/images/banner.jpg"
            alt="estee advisors"
          />
        </Grid>
        <Grid item xs={12}>
          <Grid container justify="center">
            {[
              "/images/about-us-banner.jpg",
              "/images/estee.gif",
              "/images/about-us-banner.jpg",
            ].map((value, i) => (
              <Grid xs={12} sm={4} key={i} item>
                <Grid container justify="center">
                  <Paper className={classes.paper}>
                    <img className={classes.img} src={value} alt={value} />
                  </Paper>
                </Grid>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default Home;
