import axios from 'axios';

let API_BASE_URL;

if (process.env.NODE_ENV === 'development') {
  API_BASE_URL = 'http://localhost:8081/api/v1';
} else {
  API_BASE_URL = 'https://secure-spire-80208.herokuapp.com/api/v1';
}

const apiRequest = async (
  method,
  url,
  queryParams = {},
  data = null,
  headers = {},
  onUploadProgress,
  onDownloadProgress,
  responseType = 'json',
) => {
  try {
    /* console.log(
      'New Request: ',
      method,
      API_BASE_URL + url,
      'headers: ',
      headers,
      'data: ',
      data,
      'queryParams: ',
      queryParams,
    ); */

    const r = await axios.request({
      baseURL: API_BASE_URL,
      method,
      url,
      params: queryParams,
      data,
      // headers: headersForRequesr,
      headers,
      onUploadProgress,
      onDownloadProgress,
      timeout: 3600 * 1000, // 3600 seconds
    });
    // console.log('r.data: ', r.data);
    return r.data;
  } catch (e) {
    // c onsole.log(`error apiRequest for: ${url} `, e);
    const error = new Error(e.originalMessage || 'Something went wrong, please try again!');
    error.statusCode = e.statusCode || 0;

    if (error.statusCode === 0) {
      error.message = 'Please check internet connection';
    }

    if (e.response) {
      // console.log('e.response.data: ', e.response.data);
      error.statusCode = e.response.status;
      error.message = e.response.data.message;
    }

    if (error.statusCode !== 401 && error.statusCode !== 402) {
      // handle error
    }

    if (error.statusCode === 401) {
      // handle error
    } else if (error.statusCode === 402) {
      // handle error
    }
    throw error;
  }
};

export { apiRequest };
export default apiRequest;
