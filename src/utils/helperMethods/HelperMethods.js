import { Redirect } from 'react-router-dom';
import React from 'react';
import { authToken } from '../authToken/authToken';
import store from "../../store/createStore";
import {removeUserFromStore} from "../../store/actions";

class HelperMethods {
  handleActionError = (actionType, error) => {
    return {
      type: actionType,
      payload: error.message,
      message: error.message,
    };
  };

  routeRender = (props, redirectPath, Component, authTokenName) => {
    const validToken = authToken.validateToken(authTokenName, localStorage.getItem(authTokenName));
    if (!validToken) store.dispatch(removeUserFromStore());
    return !validToken ? (
      <Redirect to={redirectPath} />
    ) : (
      <Component {...props} />
    );
  };
}

export const helperMethods = new HelperMethods();
export default helperMethods;
