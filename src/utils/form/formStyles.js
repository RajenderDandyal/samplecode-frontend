// eslint-disable-next-line
const formStyles = theme => ({
  layout: {
    display: 'block', // Fix IE 11 issue.
    boxSizing: 'border-box',
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up('xs')]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    [theme.breakpoints.down('xs')]: {
      width: '90%',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  errMsg: {
    color: theme.palette.error.main,
    fontSize: theme.typography.caption.fontSize,
  },
  paper: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(8),
    display: 'flex',
    boxSizing: 'border-box',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`,
  },
  avatar: {
    margin: theme.spacing(),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(),
  },
  submit: {
    marginTop: theme.spacing(3),
    textDecoration: 'none',
  },
  linkToRegister: {
    display: 'flex',
    flexFlow: 'column',
    fontWeight: 'bold',
    fontSize: 'medium',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export { formStyles };
export default formStyles;
