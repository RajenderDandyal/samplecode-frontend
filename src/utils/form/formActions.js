/* eslint-disable guard-for-in,no-restricted-syntax,no-underscore-dangle */
class FormActions {
  validation = (event, state) => {
    let isValid = true;
    const updatedForm = {
      ...state.formData,
      [event.target.name]: {
        ...state.formData[event.target.name],
      },
    };

    const rules = updatedForm[event.target.name].rules;
    const value = updatedForm[event.target.name].value;

    if (rules.required) {
      if (updatedForm[event.target.name].elementType === 'select') {
        isValid = value !== '' && isValid;
      } else isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
      const pattern = /^\S+@\S+(\.\S+)$/;
      isValid = pattern.test(value) && isValid;
    }
    if (rules.isNumeric) {
      const pattern = /^\d+$/;
      isValid = pattern.test(value) && isValid;
    }
    if (rules.confirmPassword) {
      const match = value === state.formData.password.value;
      isValid = match && isValid;
    }
    let completeFormShouldBeValid = true;
    for (const id in updatedForm) {
      completeFormShouldBeValid = !!(updatedForm[id].isValid && updatedForm[id].touched);
    }
    updatedForm[event.target.name].isValid = isValid;
    return { formData: updatedForm, completeFormValidity: completeFormShouldBeValid };
  };

  handleChange = (event, state) => {
    const updatedForm = {
      ...state.formData,
      [event.target.name]: {
        ...state.formData[event.target.name],
        value:
          state.formData[event.target.name].elementType === 'select'
            ? event.target.value
            : event.target.value,
        touched: true,
      },
    };

    let completeFormShouldBeValid = true;
    for (const id in updatedForm) {
      completeFormShouldBeValid = !!(updatedForm[id].isValid && updatedForm[id].touched);
    }
    return { formData: updatedForm, completeFormValidity: completeFormShouldBeValid };
  };

  addSelectOptions = (data, state, category) => {
    const updatedFormData = { ...state.formData };
    const newOptions = [...state.formData[category].elementConfig.option];

    data.forEach(item => newOptions.push({ value: item._id, displayValue: item.name }));

    updatedFormData[category].elementConfig.option = newOptions;

    return { formData: updatedFormData };
  };

  clearFields = state => {
    const updatedFormData = { ...state.formData };
    for (const key in updatedFormData) {
      if (key === 'images') {
        updatedFormData[key].value = [];
      }
      updatedFormData[key].value = '';
    }
    return {
      formData: updatedFormData,
      completeFormValidity: false,
      succfromServer: true,
    };
  };
}

export const formActions = new FormActions();
export default formActions;
