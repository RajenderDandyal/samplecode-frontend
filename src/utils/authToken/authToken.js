/* eslint-disable no-underscore-dangle */
import axios from 'axios';
// eslint-disable-next-line camelcase
import jwt_decode from 'jwt-decode';
import isEmpty from 'lodash/isEmpty';
import store from '../../store/createStore';
import { userDetail } from '../../store/actions';

class AuthToken {
  // validate token from local storage
  validateToken = (tokenName, bearerToken) => {
    let isValidToken;
    // if no token ... after sign in load token for private route auth
    bearerToken = bearerToken || localStorage.getItem(tokenName);
    // console.log(tokenName, bearerToken)
    if (bearerToken && typeof bearerToken === 'string') {
      const decoded = this.decodeToken(bearerToken);
      const expired = decoded.exp < Date.now() / 1000;
      // console.log("decoded token: "+decoded, "expired: "+expired)
      if (decoded && !expired) {
        axios.defaults.headers.common.Authorization = bearerToken;
        // console.log(store.getState().user);
        // if no user detail in store then load user detail
        if (isEmpty(store.getState().user.user)) {
          store.dispatch(userDetail(decoded._id));
        }
        isValidToken = true;
      } else {
        this.removeAuthToken(tokenName);
        console.log('redirect and remove token');
        // window.location.href = '/sign_in';
        isValidToken = false;
      }
    } else {
      // console.log("redirect");
      // window.location.href = '/sign_in'
      isValidToken = false;
    }
    return isValidToken;
  };

  decodeToken = bearerToken => {
    const token = bearerToken.split(' ')[1].trim();
    // console.log("validate token--", name, token)
    const decoded = jwt_decode(token);
    // console.log(decoded)
    return decoded;
  };

  setAuthToken = (tokenName, token) => {
    if (token && typeof token === 'string') {
      // add to local storage
      localStorage.setItem(tokenName, token);
      // Apply to every request
      axios.defaults.headers.common.Authorization = token;
    }
  };

  removeAuthToken = tokenName => {
    // remove token from local storage
    if (tokenName && typeof tokenName === 'string') {
      localStorage.removeItem(tokenName);
      // Delete auth header
      delete axios.defaults.headers.common.Authorization;
    }
  };
}

export const authToken = new AuthToken();
export default authToken;
