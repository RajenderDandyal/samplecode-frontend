/* eslint-disable no-underscore-dangle,max-len */
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import promiseMiddleware from 'redux-promise';
import { user, userList, selectedUserDetail, globalMsg } from './reducers';

const initialState = {};

const composeEnhancers =
  process.env.NODE_ENV === 'development'
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : null || compose;

const rootReducer = combineReducers({
  user,
  userList,
  selectedUserDetail,
  globalMsg,
});

const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(applyMiddleware(promiseMiddleware)),
);
export default store;
