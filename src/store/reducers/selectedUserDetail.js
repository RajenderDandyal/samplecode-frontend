import * as actionTypes from '../actions/actionTypes';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SELECTED_USER_DETAIL:
      return action.payload;
    default:
      return state;
  }
};
