import * as actionTypes from '../actions/actionTypes';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_LIST:
      return action.payload;
    case actionTypes.DELETE_USER_LIST:
      return action.payload;
    default:
      return state;
  }
};
