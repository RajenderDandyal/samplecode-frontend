import * as actionTypes from '../actions/actionTypes';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GLOBAL_ERROR_MSG:
      return {
        ...state,
        errorMsg: action.payload.message,
      };
    case actionTypes.GLOBAL_SUCCESS_MSG:
      return {
        ...state,
        successMsg: action.payload.message,
      };
    case actionTypes.GLOBAL_WARNING_MSG:
      return {
        ...state,
        warningMsg: action.payload.message,
      };
    default:
      return state;
  }
};
