import isEmpty from 'lodash/isEmpty';

import * as actionTypes from '../actions/actionTypes';

const initialState = {
  isAuthenticated: false,
  isAdmin: false,
  user: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_DETAIL:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        isAdmin: action.payload.role ? action.payload.role !== 0 : false,
        user: action.payload,
      };
    case actionTypes.SIGNIN_OUT:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        isAdmin: action.payload.role ? action.payload.role !== 0 : false,
        user: action.payload,
      };
    default:
      return state;
  }
};
