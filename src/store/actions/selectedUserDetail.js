import isEmpty from 'lodash/isEmpty';
import { apiRequest, helperMethods } from '../../utils';
import * as actionTypes from './actionTypes';

export const selectedUserDetail = async id => {
  let res;
  try {
    res = await apiRequest('get', `/user/detail/${id}`);
    return {
      type: actionTypes.SELECTED_USER_DETAIL,
      payload: isEmpty(res.body) ? [] : res.body,
    };
  } catch (e) {
    // handle error
    return helperMethods.handleActionError(actionTypes.GLOBAL_ERROR_MSG, e);
  }
};

export const deleteSelectedUserDetail = () => {
  return {
    type: actionTypes.SELECTED_USER_DETAIL,
    payload: [],
  };
};
