/* eslint-disable import/prefer-default-export */
import isEmpty from 'lodash/isEmpty';
import * as actionTypes from './actionTypes';
import { helperMethods, apiRequest } from '../../utils';

export const userList = async (skip, limit, presentList = []) => {
  let res;
  try {
    res = await apiRequest('get', '/user/list', { skip, limit });
    return {
      type: actionTypes.USER_LIST,
      payload:
        !isEmpty(res.body) && Array.isArray(res.body)
          ? [...presentList, ...res.body]
          : [...presentList],
      noMoreToLoad: !!isEmpty(res.body),
    };
  } catch (e) {
    // handle error
    return helperMethods.handleActionError(actionTypes.GLOBAL_ERROR_MSG, e);
  }
};

export const deleteUserList = async () => {
  return {
    type: actionTypes.DELETE_USER_LIST,
    payload: [],
  };
};
