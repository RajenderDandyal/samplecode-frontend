import isEmpty from 'lodash/isEmpty';
import { apiRequest } from '../../utils/axios/apiRequest';
import * as actionTypes from './actionTypes';
import { authToken, helperMethods } from '../../utils';

export const signUpUser = async data => {
  let res;
  try {
    res = await apiRequest('post', '/user/sign_up', null, data);
    return {
      type: actionTypes.SIGNUP_USER,
      payload: res,
    };
  } catch (e) {
    // handle error
    return helperMethods.handleActionError(actionTypes.GLOBAL_ERROR_MSG, e);
  }
};
export const signInUser = async data => {
  let res;
  try {
    res = await apiRequest('post', '/user/sign_in', null, data);
    return {
      type: actionTypes.SIGNIN_USER,
      payload: !isEmpty(res.body) ? res.body : [],
    };
  } catch (e) {
    // handle error
    return helperMethods.handleActionError(actionTypes.GLOBAL_ERROR_MSG, e);
  }
};
export const signOut = async id => {
  try {
    await apiRequest('get', `/user/sign_out/${id}`, null);
    authToken.removeAuthToken('jwt_auth');
    return {
      type: actionTypes.SIGNIN_OUT,
      payload: [],
    };
  } catch (e) {
    // handle error
    return helperMethods.handleActionError(actionTypes.GLOBAL_ERROR_MSG, e);
  }
};

export const removeUserFromStore = async () => {
  try {
    authToken.removeAuthToken('jwt_auth');
    return {
      type: actionTypes.SIGNIN_OUT,
      payload: [],
    };
  } catch (e) {
    // handle error
    return helperMethods.handleActionError(actionTypes.GLOBAL_ERROR_MSG, e);
  }
};

export const userDetail = async id => {
  // console.log("user detail", id)
  let res;
  try {
    res = await apiRequest('get', `/user/detail/${id}`);
    return {
      type: actionTypes.USER_DETAIL,
      payload: res.body,
    };
  } catch (e) {
    // handle error
    return helperMethods.handleActionError(actionTypes.GLOBAL_ERROR_MSG, e);
  }
};
