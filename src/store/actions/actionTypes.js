// user
export const SIGNUP_USER = 'SIGNUP_USER';
export const SIGNIN_USER = 'SIGNIN_USER';
export const SIGNIN_OUT = 'SIGNIN_OUT';
export const USER_DETAIL = 'USER_DETAIL';
// user dashboard
export const USER_LIST = 'USERS_LIST';
export const SELECTED_USER_DETAIL = 'SELECTED_USER_DETAIL';
export const DELETE_USER_LIST = 'DELETE_USER_LIST';

// global messages handling to display Snackbar/toast component
export const GLOBAL_ERROR_MSG = 'GLOBAL_ERROR_MSG';
export const GLOBAL_SUCCESS_MSG = 'GLOBAL_SUCCESS_MSG';
export const GLOBAL_WARNING_MSG = 'GLOBAL_SUCCESS_MSG';
