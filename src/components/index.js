import Layout from './Layout/Layout';
import PageLoader from './PageLoader/PageLoader';

export { PageLoader, Layout };
