/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import isEmpty from 'lodash/isEmpty';

import PropTypes from 'prop-types';
import SideDrawer from '../SideDrawer/sideDrawer';
import * as actionCreators from '../../../store/actions';

const styles = theme => ({
  root: {
    width: '100%',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  link: {
    textDecoration: 'none',
    fontWeight: 'bold',
    color: theme.palette.primary.contrastText,
  },
});

class Header extends Component {
  state = {
    open: false,
    appBarHeight: 64,
    routes: [
      {
        title: 'Home',
        path: '/',
        authUser: true,
        publicRoutes: true,
      },
      {
        title: 'Sign up',
        path: '/sign_up',
        authUser: false,
        publicRoutes: true,
      },
      {
        title: 'Sign in',
        path: '/sign_in',
        authUser: false,
        publicRoutes: true,
      },
      {
        title: 'DashBoard',
        path: '/user_dashboard',
        authUser: true,
        publicRoutes: false,
      },
    ],
  };

  componentDidMount() {
    const appBarHeight = document.getElementById('appBar').offsetHeight;
    this.setState({ appBarHeight });
  }

  toggleDrawer = () => {
    this.setState(prevState => {
      return { open: !prevState.open };
    });
  };

  signOut = async () => {
    const { dispatch, userId, history } = this.props;
    if (!isEmpty(userId)) {
      await dispatch(actionCreators.signOut()).then(() => {
        history.push('/');
      });
    }
  };

  render() {
    const { classes, isAuthenticated } = this.props;
    const { open, routes, appBarHeight } = this.state;

    return (
      <React.Fragment>
        <div id="appBar" className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              <IconButton
                onClick={this.toggleDrawer}
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="menu"
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                Estee Advisors
              </Typography>
              {!isAuthenticated ? (
                <Button color="inherit">
                  <Link to="/sign_in" className={classes.link}>
                    Sign in
                  </Link>
                </Button>
              ) : (
                <Button onClick={this.signOut} color="inherit" className={classes.link}>
                  Sign out
                </Button>
              )}
            </Toolbar>
          </AppBar>
        </div>
        <SideDrawer
          appBarHeight={appBarHeight}
          isAuthenticated={isAuthenticated}
          open={open}
          toggleDrawer={this.toggleDrawer}
          routes={routes}
        />
      </React.Fragment>
    );
  }
}
Header.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  userId: PropTypes.string.isRequired,
};
const mapStateToProps = state => {
  return {
    isAuthenticated: !isEmpty(state.user.user) ? state.user.isAuthenticated : false,
    userId: !isEmpty(state.user.user) ? state.user.user[0]._id : '',
  };
};
export default withRouter(connect(mapStateToProps)(withStyles(styles)(Header)));
