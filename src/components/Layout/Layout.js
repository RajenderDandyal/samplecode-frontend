import React, { Component } from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';

import Header from './Header/Header';
import Footer from './Footer/footer';

class Layout extends Component {
  render() {
    const { children } = this.props;
    // console.log("layout===========", globalErrMsg)
    return (
      <div className="container">
        <Header />
        <div style={{ minHeight: '80vh' }}>{children}</div>
        <Footer />
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    globalMsg: !isEmpty(state.globalMsg) ? state.globalMsg : {},
  };
};

export default connect(mapStateToProps)(Layout);
