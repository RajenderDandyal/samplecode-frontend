import React from 'react';
import Typography from '@material-ui/core/Typography';

const styles = {
  container: {
    width: '100%',
    height: '200px',
    backgroundColor: '#070D59',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  wrapperCopyWrite: { width: '50%' },
  copyWrite: {
    color: '#fff',
    textAlign: 'center',
  },
};
const footer = () => {
  return (
    <div style={styles.container}>
      <div style={styles.wrapperCopyWrite}>
        <Typography style={styles.copyWrite} variant="body1">
          &#169; {new Date().getFullYear()} Estee Advisors Pvt Ltd
        </Typography>
      </div>
    </div>
  );
};

export default footer;
