/* eslint-disable jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const styles = theme => {
  return {
    link: {
      textDecoration: 'none',
      fontWeight: 'bold',
      color: 'black',
    },
    appBar: {
      width: 250,
      backgroundColor: theme.palette.primary.main,
      color: '#fff',
      fontWeight: 'bold',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  };
};

function sideDrawer({ open, toggleDrawer, routes, isAuthenticated, classes, appBarHeight }) {
  const returnRoute = route => {
    return (
      <Link className={classes.link} to={route.path} key={route.title}>
        <ListItem button>
          <ListItemText primary={route.title} />
        </ListItem>
      </Link>
    );
  };
  const authRoutes = () => {
    return routes
      .filter(route => route.authUser === true)
      .map(route => {
        return returnRoute(route);
      });
  };
  const publicRoutes = () => {
    return routes
      .filter(route => route.publicRoutes === true)
      .map(route => {
        return returnRoute(route);
      });
  };

  return (
    <Drawer open={open} onClose={toggleDrawer}>
      <div className={classes.appBar} style={{ height: appBarHeight }}>
        <Typography variant="h6">Estee Advisors</Typography>
      </div>
      <div onClick={toggleDrawer}>
        <List component="nav">{!isAuthenticated ? publicRoutes() : authRoutes()}</List>
      </div>
    </Drawer>
  );
}

sideDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  toggleDrawer: PropTypes.func.isRequired,
  routes: PropTypes.array.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  appBarHeight: PropTypes.number.isRequired,
};
export default withStyles(styles)(sideDrawer);
