import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const pageLoader = () => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
      }}
    >
      <CircularProgress size={40} thickness={3} />
    </div>
  );
};

export default pageLoader;
