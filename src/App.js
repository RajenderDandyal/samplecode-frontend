import React, { lazy, Suspense } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';

import { Layout, PageLoader } from './components';
import { helperMethods, authToken } from './utils';

// Routes
const Home = lazy(() => import('./pages/Home/Home'));
const SignUp = lazy(() => import('./pages/SignUp/SignUp'));
const SignIn = lazy(() => import('./pages/SignIn/SignIn'));
const NoMatchPage = lazy(() => import('./pages/NoMatchPage/NoMatchPage'));
const UserDashboard = lazy(() => import('./pages/UserDashboard/UserDashboard'));
const UserDetail = lazy(() => import('./pages/UserDetail/UserDetail'));

class App extends React.Component {
  constructor(props) {
    super(props);
    this.authTokenName = 'jwt_auth';
    authToken.validateToken(this.authTokenName, localStorage.getItem(this.authTokenName));
  }

  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Layout>
          <Suspense fallback={<PageLoader />}>
            <Switch>
              {/* private routes */}
             {/* <Route
                path="/privateRoute"
                render={props =>
                  helperMethods.routeRender(props, '/sign_in', Home, this.authTokenName)
                }
              />
              <Route
                path="/user_dashboard"
                render={props =>
                  helperMethods.routeRender(props, '/sign_in', UserDashboard, this.authTokenName)
                }
              />
              <Route
                path="/detail/:id"
                render={props =>
                  helperMethods.routeRender(props, '/sign_in', UserDetail, this.authTokenName)
                }
              />*/}
              {/* public routes */}
              <Route path="/user_dashboard" component={UserDashboard} />
              <Route path="/detail/:id" component={UserDetail} />

              <Route path="/sign_up" component={SignUp} />
              <Route path="/sign_in" component={SignIn} />
              <Route exact path="/" component={Home} />
              <Route component={NoMatchPage} />
            </Switch>
          </Suspense>
        </Layout>
      </React.Fragment>
    );
  }
}

export default withRouter(App);
